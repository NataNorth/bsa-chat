import React from 'react';
import {Button} from 'semantic-ui-react'
import './Message.css'

class Message extends React.Component {
    constructor() {
        super()
        this.state = {
            text: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.setState({
            text: this.props.message.text
        })
    }

    deleteMessage(e){
        e.preventDefault();
        this.props.deleteMessage(this.props.message.id);
    }

    editMessage(e, id) {
        e.preventDefault();
        var messageItem = document.getElementById(id);
        messageItem.getElementsByTagName('form')[0].style.display = "block";
        
    }

    handleChange(e) {
        this.setState({
            text: e.target.value
        });
    }

    handleSubmit(e){
        e.preventDefault();
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+'T'+time+'Z';
        document.getElementById('editForm').style.display = 'none';
        this.props.editMessage(this.props.message.id, dateTime, this.state.text);
    }

    handleClick(e) {
        e.preventDefault();
        if (this.props.currentUserId !== this.props.message.userId) {
            e.target.classList.toggle('active');
        }
    }

    render() {
        return (
            <div className='message' id={this.props.message.id}>
                <div>
                    <img src = {this.props.message.avatar} alt='avatar'/>
                </div>
                <div className='messageText'>
                    {this.props.message.text}
                </div>
                <button class="ui icon button" className='likeButton'
                        onClick={this.handleClick}>
                    <i class="heart icon"></i>
                </button>
                <form id='editForm'
                    onSubmit={this.handleSubmit}
                    style={{display: 'none'}}
                    >
                        <input
                        onChange={this.handleChange}
                        name='editInput'
                        type='text'
                        value={this.state.text}
                    />
                    <button type='submit'>Submit</button>
                </form>
                
                {this.props.currentUserId === this.props.message.userId ?
                    <div className='mutationForm'>
                        <button 
                            name="edit"
                            onClick={(e) => this.editMessage(e, this.props.message.id)}
                        >Edit</button>
                        <button 
                            name='delete'
                            onClick={(e) => this.deleteMessage(e)}
                        >Delete</button>
                    </div> : <div/>}
            </div>
        )
    }
}

export default Message;
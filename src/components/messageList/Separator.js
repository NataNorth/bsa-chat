import React from 'react';
import {Divider} from 'semantic-ui-react'

class Separator extends React.Component {

    getText(date) {
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        var currDate = new Date(date);
        if (currDate === yesterday) {
            return 'Yesterday'
        }
        return currDate.toDateString();
    }

    render() {
        return(
            <Divider horizontal>{this.getText(this.props.date)}</Divider>
        )
    }
}

export default Separator;
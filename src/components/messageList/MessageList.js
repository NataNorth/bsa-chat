import React from 'react';
import Message from './Message'
import Separator from './Separator'
import './MessageList.css'


class MessageList extends React.Component {

    render() {
        var previousDate = '';
      return ( this.props.loading ? 
        <div class="ui active centered inline loader"></div> :
        <div className ="messageList"> 
            {this.props.messages.map(message => {
                const currDate = message.createdAt.split('T')[0];
                var element = '';
                if (previousDate && previousDate !== currDate ) {
                    element =
                        <div>
                            <Separator date = {previousDate}/>
                            <Message message = {message} currentUserId={this.props.currentUserId}
                            deleteMessage= {this.props.deleteMessage} editMessage = {this.props.editMessage}/>
                        </div>
                } else {
                    element =
                        <Message message = {message} currentUserId={this.props.currentUserId}
                        deleteMessage= {this.props.deleteMessage} editMessage = {this.props.editMessage}/>
                }
                previousDate = currDate;
                return (element)
            })}              
       </div>
      )
    }
}

export default MessageList;
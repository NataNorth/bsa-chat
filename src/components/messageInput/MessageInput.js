import React from 'react';
import './MessageInput.css'

class MessageInput extends React.Component {
    constructor() {
        super()
        this.state = {
            message: {
                id: '',
                text:'',
                createdAt: null
            }
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e) {
        const messageId = ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,c=>(c^crypto.getRandomValues(new Uint8Array(1))[0]&15 >> c/4).toString(16));
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+'T'+time+'Z';
        this.setState({
            message: {
                id: messageId,
                text: e.target.value,
                createdAt: dateTime
            }
        })
    }

    handleSubmit(e) {
        e.preventDefault()
        this.props.sendMessage(this.state.message)
        this.setState({
            message: {
                id: '',
                text: '',
                createdAt: null
            }
        })
    }

    render() {
        return (
            <form
                onSubmit={this.handleSubmit}
                className="messageInput">
                <input
                    onChange={this.handleChange}
                    value={this.state.message.text}
                    placeholder="Message"
                    type="text" />
                <button
                    type='submit'
                >Send</button>
            </form>
        )
    }
  }

export default MessageInput;
import React from 'react';
import './Header.css'

class Header extends React.Component {

    getLastDate(messages){
        var lastDate = messages[messages.length - 1].createdAt
        var day = new Date(lastDate.split('T')[0]);
        var time = lastDate.split('T')[1].slice(0, -1).split('.')[0];
        // const options = { year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"}
        // return new Date(lastDate).toLocaleDateString('eng', options);
        return 'Last message ' + day.toDateString() + ' ' + time
    }

    render() {
        return (
            <div className='header'>
                <p className="title">Whazup</p>
                {this.props.loading ? <br/>: <p className="date">{this.getLastDate(this.props.messages)}</p>}
            </div>
        )
    }
    
  }

export default Header;
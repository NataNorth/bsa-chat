import React from 'react';
import MessageList from './components/messageList/MessageList'
import MessageInput from './components/messageInput/MessageInput'
import Header from './components/header/Header'
import logo from './logo.svg';
import './App.css';

class Chat extends React.Component {
  constructor() {
      super()
      this.state = {
          loading: true,
          messages: {},
          currentUser: {}
      }
    this.sendMessage = this.sendMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.editMessage = this.editMessage.bind(this);
    this.getLastDate = this.getLastDate.bind(this);
  }

  componentDidMount() {
      this.setState({loading: true})
      fetch("https://api.npoint.io/21df9f7846c2077083ab")
          .then(response => response.json())
          .then(data => {
              this.setState({
                  loading: false,
                  messages: data,
                  currentUser: {
                    id: data[0].userId,
                    name: data[0].user,
                    avatar: data[0].avatar
                  }
              })
          })
  }

  sendMessage(message) {
    const newMessage = {
      id: message.id,
      text: message.text,
      user: this.state.currentUser.name,
      avatar: this.state.currentUser.avatar,
      userId: this.state.currentUser.id,
      editedAt: "",
      createdAt: message.createdAt
    }
    this.setState({
      messages: [...this.state.messages, newMessage]
    })
  }

  deleteMessage(id) {
    var newMessages = this.state.messages.filter(message => {return (message.id !== id || message.userId !== this.state.currentUser.id)})
    this.setState({
      messages: newMessages
    })
  }

  editMessage(id, date, text) {
    var newMessages = this.state.messages.map(message => {
      if (message.id === id && message.userId === this.state.currentUser.id) {
        message.editedAt = date;
        message.text = text;
      }
      return message;})
    this.setState({
      messages: newMessages
    })
  }

  getLastDate(){
    alert(this.state.messages[this.state.messages.length - 1].createdAt)
      return this.state.messages[this.state.messages.length - 1].createdAt
  }

  render() {
      return (
          <div className="chat">  
          <Header loading = {this.state.loading} messages={this.state.messages} />
          <MessageList loading = {this.state.loading} messages = {this.state.messages} 
          deleteMessage = {this.deleteMessage} editMessage={this.editMessage} currentUserId={this.state.currentUser.id}/>
          <MessageInput sendMessage = {this.sendMessage}/>
      </div>
      )
  }

  
}



// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

 export default Chat;
